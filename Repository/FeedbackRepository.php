<?php

namespace lst\CompanyBundle\Repository;

use lst\CompanyBundle\Entity\Feedback;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Feedback|null find($id, $lockMode = null, $lockVersion = null)
 * @method Feedback|null findOneBy(array $criteria, array $orderBy = null)
 * @method Feedback[]    findAll()
 * @method Feedback[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FeedbackRepository extends ServiceEntityRepository
{
    /** @var \Doctrine\ORM\EntityManager */
    private $em;

    public function __construct(RegistryInterface $registry)
    {
        $this->em = $registry->getEntityManager();
        parent::__construct($registry, Feedback::class);
    }
}
