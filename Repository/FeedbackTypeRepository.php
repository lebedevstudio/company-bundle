<?php

namespace lst\CompanyBundle\Repository;

use lst\CompanyBundle\Entity\FeedbackType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method FeedbackType|null find($id, $lockMode = null, $lockVersion = null)
 * @method FeedbackType|null findOneBy(array $criteria, array $orderBy = null)
 * @method FeedbackType[]    findAll()
 * @method FeedbackType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FeedbackTypeRepository extends ServiceEntityRepository
{
    /** @var \Doctrine\ORM\EntityManager */
    private $em;

    public function __construct(RegistryInterface $registry)
    {
        $this->em = $registry->getEntityManager();
        parent::__construct($registry, FeedbackType::class);
    }
}
