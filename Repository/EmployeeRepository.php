<?php

namespace lst\CompanyBundle\Repository;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use lst\CompanyBundle\Entity\Employee;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Employee|null find($id, $lockMode = null, $lockVersion = null)
 * @method Employee|null findOneBy(array $criteria, array $orderBy = null)
 * @method Employee[]    findAll()
 * @method Employee[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmployeeRepository extends ServiceEntityRepository
{
    /** @var EntityManager */
    private $em;

    public function __construct(RegistryInterface $registry)
    {
        $this->em = $registry->getEntityManager();

        parent::__construct($registry, Employee::class);
    }

    /**
     * @param Employee $employee
     *
     * @return Employee
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function persist(Employee $employee): Employee
    {
        $entry = $this->em->merge($employee);
        $this->em->flush();

        return $entry;
    }

    /**
     * @param  Employee  $employee
     *
     * @return Employee
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete(Employee $employee): Employee
    {
        $this->em->remove($employee);
        $this->em->flush();

        return $employee;
    }
}
