<?php

declare(strict_types=1);

namespace lst\CompanyBundle\Controller;

use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use lst\CompanyBundle\Entity\Department;
use lst\CompanyBundle\Entity\Employee;
use lst\CompanyBundle\Repository\EmployeeRepository;
use lst\CoreBundle\Abstractions\AbstractController;
use lst\CoreBundle\Service\Operations\Operations;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class EmployeeDepartmentController extends AbstractController
{
    /** @var EmployeeRepository */
    private $employeeRepository;
    /** @var Operations */
    protected $operations;

    public function __construct(
        Operations $operations,
        NormalizerInterface $normalizer,
        RequestStack $request,
        EmployeeRepository $employeeRepository)
    {
        $this->operations = $operations;
        $this->employeeRepository = $employeeRepository;

        parent::__construct($normalizer, $request);
    }

    /**
     * @Route(
     *     "/company/employees/{employee}/department/{department}",
     *     name="company.employee.department.add",
     *     methods={"POST"},
     *     requirements={"employees"="\d+", "department"="\d+"}
     * )
     *
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Employee $employee
     * @param Department $department
     *
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function addDepartmentToEmployee(Employee $employee, Department $department): JsonResponse
    {
        $employee->getDepartments()->add($department);
        $this->employeeRepository->persist($employee);

        return new JsonResponse([
            'result' => 'OK'
        ], $this->responseStatus);
    }

    /**
     * @Route(
     *     "/company/employees/{employee}/department/{department}",
     *     name="company.employee.department.unlink",
     *     methods={"DELETE"},
     *     requirements={"employee"="\d+", "department"="\d+"}
     * )
     *
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Department $department
     * @param Employee $employee
     *
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function unlinkDepartmentFromEmployee(Department $department, Employee $employee): JsonResponse
    {
        $employee->getDepartments()->removeElement($department);
        $this->employeeRepository->persist($employee);

        return new JsonResponse([
            'result' => 'OK'
        ], $this->responseStatus);
    }
}