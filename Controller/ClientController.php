<?php

declare(strict_types=1);

namespace lst\CompanyBundle\Controller;

use lst\CompanyBundle\Entity\Client;
use lst\CoreBundle\Abstractions\AbstractController;
use lst\CoreBundle\Service\Operations\Operations;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class ClientController extends AbstractController
{
    /** @var Operations */
    protected $operations;
    /** @var string */
    private $entity = Client::class;
    /** @var string */
    private $entitySingleKey = Client::SINGLE_KEY;
    /** @var string */
    private $entityMultipleKey = Client::MULTIPLE_KEY;

    public function __construct(Operations $operations, NormalizerInterface $normalizer, RequestStack $request)
    {
        $this->operations = $operations;

        parent::__construct($normalizer, $request);
    }

    /**
     * @Route(
     *     "/company/clients",
     *     name="company.client.get.list",
     *     methods={"GET"}
     * )
     *
     * @return JsonResponse
     */
    public function listEntity(): JsonResponse
    {
        return $this->list($this->entity, $this->entityMultipleKey);
    }

    /**
     * @Route(
     *     "/company/clients/{id}",
     *     name="company.client.get.id",
     *     methods={"GET"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @param Client $entity
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function getEntity(Client $entity): JsonResponse
    {
        return new JsonResponse([
            $this->entitySingleKey => $this->normalizer->normalize($entity, 'array', [
                'groups' => $this->serializationGroups
            ])
        ], $this->responseStatus);
    }

    /**
     * @Route(
     *     "/company/clients",
     *     name="company.client.create",
     *     methods={"POST"}
     * )
     * @IsGranted("ROLE_ADMIN")
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function createEntity(): JsonResponse
    {
        return $this->persist(
            $this->entity,
            $this->entitySingleKey,
            $this->request->getContent(),
            $this->request->getMethod()
        );
    }

    /**
     * @Route(
     *     "/company/clients/{id}",
     *     name="company.client.update",
     *     methods={"PUT"},
     *     requirements={"id"="\d+"}
     * )
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Client $entity
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function updateEntity(Client $entity): JsonResponse
    {
        return $this->persist(
            $this->entity,
            $this->entitySingleKey,
            $this->request->getContent(),
            $this->request->getMethod()
        );
    }

    /**
     * @Route(
     *     "/company/clients/{id}",
     *     name="company.client.delete",
     *     methods={"DELETE"},
     *     requirements={"id"="\d+"}
     * )
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Client $entity
     *
     * @return JsonResponse
     */
    public function deleteEntity(Client $entity): JsonResponse
    {
        return $this->delete($entity);
    }
}
