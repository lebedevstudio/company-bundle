<?php

declare(strict_types=1);

namespace lst\CompanyBundle\Controller;

use lst\CompanyBundle\Entity\Vacancy;
use lst\CoreBundle\Abstractions\AbstractController;
use lst\CoreBundle\Service\Operations\Operations;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class VacancyController extends AbstractController
{
    /** @var Operations */
    protected $operations;

    /** @var string */
    private $entity = Vacancy::class;
    /** @var string */
    private $entitySingleKey = Vacancy::SINGLE_KEY;
    /** @var string */
    private $entityMultipleKey = Vacancy::MULTIPLE_KEY;

    public function __construct(Operations $operations, NormalizerInterface $normalizer, RequestStack $request)
    {
        $this->operations = $operations;

        parent::__construct($normalizer, $request);
    }

    /**
     * @Route(
     *     "/company/vacancies",
     *     name="company.vacancy.list",
     *     methods={"GET"}
     * )
     *
     * @return JsonResponse
     */
    public function listEntity(): JsonResponse
    {
        return $this->list($this->entity, $this->entityMultipleKey);
    }

    /**
     * @Route(
     *     "/company/vacancies/{id}",
     *     name="company.vacancy.get",
     *     methods={"GET"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @param Vacancy $entity
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function getEntity(Vacancy $entity): JsonResponse
    {
        return new JsonResponse([
            $this->entitySingleKey => $this->normalizer->normalize($entity, 'array', [
                'groups' => $this->serializationGroups
            ])
        ], $this->responseStatus);
    }

    /**
     * @Route(
     *     "/company/vacancies",
     *     name="company.vacancy.create",
     *     methods={"POST"}
     * )
     * @IsGranted("ROLE_ADMIN")
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function createEntity(): JsonResponse
    {
        return $this->persist(
            $this->entity,
            $this->entitySingleKey,
            $this->request->getContent(),
            $this->request->getMethod()
        );
    }

    /**
     * @Route(
     *     "/company/vacancies/{id}",
     *     name="company.vacancy.update",
     *     methods={"PUT"},
     *     requirements={"id"="\d+"}
     * )
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Vacancy $entity
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function updateEntity(Vacancy $entity): JsonResponse
    {
        return $this->persist(
            $this->entity,
            $this->entitySingleKey,
            $this->request->getContent(),
            $this->request->getMethod()
        );
    }

    /**
     * @Route(
     *     "/company/vacancies/{id}",
     *     name="company.vacancy.delete",
     *     methods={"DELETE"},
     *     requirements={"id"="\d+"}
     * )
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Vacancy $entity
     *
     * @return JsonResponse
     */
    public function deleteEntity(Vacancy $entity): JsonResponse
    {
        return $this->delete($entity);
    }
}
