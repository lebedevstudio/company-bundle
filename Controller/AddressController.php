<?php

declare(strict_types=1);

namespace lst\CompanyBundle\Controller;

use lst\CoreBundle\Abstractions\AbstractController;
use lst\CompanyBundle\Entity\Address;
use lst\CoreBundle\Service\Operations\Operations;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class AddressController extends AbstractController
{
    /** @var Operations */
    protected $operations;
    /** @var string */
    private $entity = Address::class;
    /** @var string */
    private $entitySingleKey = Address::SINGLE_KEY;
    /** @var string */
    private $entityMultipleKey = Address::MULTIPLE_KEY;

    public function __construct(Operations $operations, NormalizerInterface $normalizer, RequestStack $request)
    {
        $this->operations = $operations;

        parent::__construct($normalizer, $request);
    }

    /**
     * @Route(
     *     "/company/addresses",
     *     name="company.address.list",
     *     methods={"GET"}
     * )
     *
     * @return JsonResponse
     */
    public function listEntity(): JsonResponse
    {
        return $this->list($this->entity, $this->entityMultipleKey);
    }

    /**
     * @Route(
     *     "/company/addresses/{id}",
     *     name="company.address.get",
     *     methods={"GET"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @param Address $entity
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function getEntity(Address $entity): JsonResponse
    {
        return new JsonResponse([
            $this->entitySingleKey => $this->normalizer->normalize($entity, 'array', [
                'groups' => $this->serializationGroups
            ])
        ], $this->responseStatus);
    }

    /**
     * @Route(
     *     "/company/addresses",
     *     name="company.address.create",
     *     methods={"POST"}
     * )
     * @IsGranted("ROLE_ADMIN")
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function createEntity(): JsonResponse
    {
        return $this->persist(
            $this->entity,
            $this->entitySingleKey,
            $this->request->getContent(),
            $this->request->getMethod()
        );
    }

    /**
     * @Route(
     *     "/company/addresses/{id}",
     *     name="company.address.update",
     *     methods={"PUT"},
     *     requirements={"id"="\d+"}
     * )
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Address $entity
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function updateEntity(Address $entity): JsonResponse
    {
        return $this->persist(
            $this->entity,
            $this->entitySingleKey,
            $this->request->getContent(),
            $this->request->getMethod()
        );
    }

    /**
     * @Route(
     *     "/company/addresses/{id}",
     *     name="company.address.delete",
     *     methods={"DELETE"},
     *     requirements={"id"="\d+"}
     * )
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Address $entity
     *
     * @return JsonResponse
     */
    public function deleteEntity(Address $entity): JsonResponse
    {
        return $this->delete($entity);
    }
}