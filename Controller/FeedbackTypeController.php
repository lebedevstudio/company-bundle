<?php

declare(strict_types=1);

namespace lst\CompanyBundle\Controller;

use lst\CompanyBundle\Entity\FeedbackType;
use lst\CoreBundle\Abstractions\AbstractController;
use lst\CoreBundle\Service\Operations\Operations;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class FeedbackTypeController extends AbstractController
{
    /** @var Operations */
    protected $operations;

    public function __construct(Operations $operations, NormalizerInterface $normalizer, RequestStack $request)
    {
        $this->operations = $operations;

        parent::__construct($normalizer, $request);
    }

    /**
     * @Route("/company/feedback/types", name="company.feedback.type.list", methods={"GET"})
     */
    public function listFeedBackTypes() : JsonResponse
    {
        return $this->list(FeedbackType::class, FeedbackType::MULTIPLE_KEY);
    }

    /**
     * @Route("/company/feedback/types/{type}", name="company.feedback.type.get", methods={"GET"})
     *
     * @param FeedbackType $type
     * @return JsonResponse
     */
    public function getFeedBackType(FeedbackType $type) : JsonResponse
    {
        return new JsonResponse([
            FeedbackType::SINGLE_KEY => $this->normalizer->normalize($type, 'array', [
                'groups' => $this->serializationGroups
            ])
        ], $this->responseStatus);
    }

    /**
     * @Route("/company/feedback/types", name="company.feedback.type.create", methods={"POST"})
     */
    public function createFeedbackType() : JsonResponse
    {
        return $this->persist(FeedbackType::class, FeedbackType::SINGLE_KEY, $this->request->getContent(), $this->request->getMethod());
    }

    /**
     * @Route("/company/feedback/types/{id}", name="company.feedback.type.update", methods={"PUT"}, requirements={"id"="\d+"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function updateFeedbackType() : JsonResponse
    {
        return $this->persist(FeedbackType::class, FeedbackType::SINGLE_KEY, $this->request->getContent(), $this->request->getMethod());
    }

    /**
     * @Route("/company/feedback/types/{id}", name="company.feedback.type.delete", methods={"DELETE"}, requirements={"id"="\d+"})
     * @IsGranted("ROLE_ADMIN")
     *
     * @param FeedbackType $feedbackType
     */
    public function deleteFeedbackType(FeedbackType $feedbackType) : JsonResponse
    {
        return $this->delete($feedbackType);
    }
}
