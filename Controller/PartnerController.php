<?php

declare(strict_types=1);

namespace lst\CompanyBundle\Controller;

use lst\CompanyBundle\Entity\Partner;
use lst\CoreBundle\Abstractions\AbstractController;
use lst\CoreBundle\Service\Operations\Operations;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Routing\Annotation\Route;

class PartnerController extends AbstractController
{
    /** @var Operations */
    protected $operations;
    /** @var string */
    private $entity = Partner::class;
    /** @var string */
    private $entitySingleKey = Partner::SINGLE_KEY;
    /** @var string */
    private $entityMultipleKey = Partner::MULTIPLE_KEY;

    public function __construct(Operations $operations, NormalizerInterface $normalizer, RequestStack $request)
    {
        $this->operations = $operations;

        parent::__construct($normalizer, $request);
    }

    /**
     * @Route(
     *     "/company/partners",
     *     name="company.partner.get.list",
     *     methods={"GET"}
     * )
     *
     * @return JsonResponse
     */
    public function listEntity(): JsonResponse
    {
        return $this->list($this->entity, $this->entityMultipleKey);
    }

    /**
     * @Route(
     *     "/company/partners/{id}",
     *     name="company.partner.get.id",
     *     methods={"GET"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @param Partner $entity
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function getEntity(Partner $entity): JsonResponse
    {
        return new JsonResponse([
            $this->entitySingleKey => $this->normalizer->normalize($entity, 'array', [
                'groups' => $this->serializationGroups
            ])
        ], $this->responseStatus);
    }

    /**
     * @Route(
     *     "/company/partners",
     *     name="company.partner.create",
     *     methods={"POST"}
     * )
     * @IsGranted("ROLE_ADMIN")
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function createEntity(): JsonResponse
    {
        return $this->persist(
            $this->entity,
            $this->entitySingleKey,
            $this->request->getContent(),
            $this->request->getMethod()
        );
    }

    /**
     * @Route(
     *     "/company/partners/{id}",
     *     name="company.partner.update",
     *     methods={"PUT"},
     *     requirements={"id"="\d+"}
     * )
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Partner $entity
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function updateEntity(Partner $entity): JsonResponse
    {
        return $this->persist(
            $this->entity,
            $this->entitySingleKey,
            $this->request->getContent(),
            $this->request->getMethod()
        );
    }

    /**
     * @Route(
     *     "/company/partners/{id}",
     *     name="company.partner.delete",
     *     methods={"DELETE"},
     *     requirements={"id"="\d+"}
     * )
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Partner $entity
     *
     * @return JsonResponse
     */
    public function deleteEntity(Partner $entity): JsonResponse
    {
        return $this->delete($entity);
    }
}