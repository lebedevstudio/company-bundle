<?php

declare(strict_types=1);

namespace lst\CompanyBundle\Controller;

use http\Exception\RuntimeException;
use lst\CompanyBundle\Entity\Employee;
use lst\CompanyBundle\Repository\EmployeeRepository;
use lst\CoreBundle\Entity\Link;
use lst\CoreBundle\Abstractions\AbstractController;
use lst\CoreBundle\Service\Operations\Operations;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class EmployeeController extends AbstractController
{
    /** @var Operations */
    protected $operations;
    /** @var EmployeeRepository */
    protected $repository;

    public function __construct(Operations $operations, NormalizerInterface $normalizer, RequestStack $request, EmployeeRepository $repository)
    {
        $this->operations = $operations;
        $this->repository = $repository;

        parent::__construct($normalizer, $request);
    }

    /**
     * @Route("/company/employees", name="company.employee.list", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function listEntity() : JsonResponse
    {
        return $this->list(Employee::class, Employee::MULTIPLE_KEY);
    }

    /**
     * @Route("/company/employees/alias/{alias}", name="company.employee.get.by.alias", methods={"GET"})
     * @param Employee $employee
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function getEntityByAlias($alias) : JsonResponse
    {
        $criteria = ['alias' => $alias];
        if ($locale = $this->filters->getLocale()) {
            $criteria['locale'] = $locale;
        }

        $employee = $this->repository->findOneBy($criteria);

        return new JsonResponse([
            Employee::SINGLE_KEY => $this->normalizer->normalize(
                $employee,'array', [
                    'groups' => $this->serializationGroups
            ]),
            Link::MULTIPLE_KEY => $this->normalizer->normalize(
                $this->getLinks($employee), 'array', [
                    'groups' => $this->serializationGroups
                ]
            ),
        ], $this->responseStatus);
    }

    /**
     * @Route("/company/employees/{employee}",
     *     name="company.employee.get",
     *     methods={"GET"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @return JsonResponse
     */
    public function getEntity(Employee $employee) :JsonResponse
    {
        return new JsonResponse([
            Employee::SINGLE_KEY => $this->normalizer->normalize($employee, 'array', [
                'groups' => $this->serializationGroups
            ]),
            Link::MULTIPLE_KEY => $this->normalizer->normalize(
                $this->getLinks($employee), 'array', [
                    'groups' => $this->serializationGroups
                ]
            ),
        ], $this->responseStatus);
    }

    /**
     * @Route("/company/employees", name="company.employee.create", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function createEntity() : JsonResponse
    {
        return $this->persist(Employee::class, Employee::SINGLE_KEY, $this->request->getContent(), $this->request->getMethod());
    }

    /**
     * @Route("/company/employees/{id}", name="company.employee.update", methods={"PUT"}, requirements={"id"="\d+"})
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Employee $employee
     *
     * @return JsonResponse
     */
    public function updateEntity(Employee $employee) : JsonResponse
    {
        return $this->persist(Employee::class, Employee::SINGLE_KEY, $this->request->getContent(), $this->request->getMethod());
    }

    /**
     * @Route("/company/employees/{id}", name="company.employee.delete", methods={"DELETE"}, requirements={"id"="\d+"})
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Employee $employee
     *
     * @return JsonResponse
     */
    public function deleteEntity(Employee $employee) : JsonResponse
    {
        return $this->delete($employee);
    }
}
