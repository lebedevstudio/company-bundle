<?php

declare(strict_types=1);

namespace lst\CompanyBundle\Controller;

use lst\CompanyBundle\Entity\Feedback;
use lst\CoreBundle\Abstractions\AbstractController;
use lst\CoreBundle\Service\Operations\Operations;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class FeedbackController extends AbstractController
{
    /** @var Operations */
    protected $operations;

    public function __construct(Operations $operations, NormalizerInterface $normalizer, RequestStack $request)
    {
        $this->operations = $operations;

        parent::__construct($normalizer, $request);
    }

    /**
     * @Route("/company/feedback", name="company.feedback.list", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     *
     * @return JsonResponse
     */
    public function listFeedback() : JsonResponse
    {
        return $this->list(Feedback::class, Feedback::MULTIPLE_KEY);
    }

    /**
     * @Route("/company/feedback/{feedback}", name="company.feedback.get", methods={"GET"}, requirements={"feedback"="\d+"})
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Feedback $feedback
     * @return JsonResponse
     */
    public function getFeedback(Feedback $feedback) : JsonResponse
    {
        return new JsonResponse([
            Feedback::SINGLE_KEY => $this->normalizer->normalize($feedback, 'array', [
                'groups' => $this->serializationGroups
            ])
        ], $this->responseStatus);
    }

    /**
     * @Route("/company/feedback", name="company.feedback.create", methods={"POST"})
     */
    public function createFeedback(\Swift_Mailer $mailer) : JsonResponse
    {
        $feedback = $this->persistAndReturnEntity(Feedback::class, Feedback::SINGLE_KEY, $this->request->getContent(), $this->request->getMethod());
        $this->sendEmail($feedback, $mailer);

        return new JsonResponse([
            Feedback::SINGLE_KEY => $this->normalizer->normalize($feedback, 'array', [
                'groups' => $this->serializationGroups
            ])
        ], $this->responseStatus);
    }

    /**
     * @Route("/company/feedback/{id}", name="company.feedback.update", methods={"PUT"}, requirements={"id"="\d+"})
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Feedback $feedback
     * @return JsonResponse
     */
    public function updateFeedback(Feedback $feedback) : JsonResponse
    {
        return $this->persist(Feedback::class, Feedback::SINGLE_KEY, $this->request->getContent(), $this->request->getMethod());
    }

    /**
     * @Route("/company/feedback/{id}", name="company.feedback.delete", methods={"DELETE"}, requirements={"id"="\d+"})
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Feedback $feedback
     * @return JsonResponse
     */
    public function deleteFeedback(Feedback $feedback) : JsonResponse
    {
        return $this->delete($feedback);
    }

    private function sendEmail(Feedback $feedback, \Swift_Mailer $mailer)
    {
        $emails = $this->getFeedbackEmails();
        $message = (new \Swift_Message())
            ->setFrom('no-reply@lebedev-studio.com')
            ->setTo($emails)
            ->setSubject($feedback->getType()->getTitle() . ' ' . $this->getParameter('app.name'))
            ->setBody($this->composeMessage($feedback));

        $mailer->send($message, $mailer);
    }


    private function getFeedbackEmails() : array
    {
        $emails = [];
        if ($s = $this->getParameter('feedback.emails')) {
            $emails = explode(';', $s);
        }

        return $emails;
    }


    private function composeMessage(Feedback $feedback) : string
    {
        $message = 'Доброго времени суток! ' . PHP_EOL . PHP_EOL;
        $message .= 'У вас есть новое сообщение на сайте: ' . $this->getParameter('app.name') . PHP_EOL;
        $message .= "В контактных данных указано:" . PHP_EOL;
        $message .= 'имя: ' . $feedback->getCaller() . PHP_EOL;
        $message .= 'email: ' . $feedback->getEmail() . PHP_EOL;
        if ($phone = $feedback->getPhone()) {
            $message .= 'телефон: ' . $phone . PHP_EOL;
        }
        if ($content = $feedback->getContent()) {
            $message .= 'cooбщение: ' . $content . PHP_EOL;
        }

        $message .= 'Подробнее на сайте. https://' . $this->getParameter('admin.domain') . '/feedback/message/' . $feedback->getId();

        return $message;
    }
}
