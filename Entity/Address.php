<?php

declare(strict_types=1);

namespace lst\CompanyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use lst\CoreBundle\Abstractions\AbstractEntity;
use lst\CoreBundle\Abstractions\Traits\Activity;
use lst\CoreBundle\Abstractions\Traits\ExternalId;
use lst\CoreBundle\Abstractions\Traits\Timestampable;
use lst\CoreBundle\Abstractions\Traits\Translatable;
use lst\CoreBundle\Entity\City;
use lst\CoreBundle\Interfaces\EntityTypeInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use lst\CoreBundle\Validator\Constraints as Asserts;

/**
 * Address Entity
 *
 * @ORM\Table(name="company_addresses")
 * @ORM\Entity(repositoryClass="lst\CompanyBundle\Repository\AddressRepository")
 */
class Address extends AbstractEntity implements EntityTypeInterface
{
    use Timestampable, Activity, Translatable, ExternalId;

    /** @var int */
    protected const ENTITY_TYPE_ID = 22;
    /** @var string */
    public const SINGLE_KEY = 'address';
    /** @var string */
    public const MULTIPLE_KEY = 'addresses';

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"basic"})
     */
    private $id;

    /**
     * @Assert\Length(max=255)
     * @Assert\NotBlank()
     * @ORM\Column(
     *     type="string",
     *     length=255,
     *     nullable=false
     * )
     *
     * @Groups({"basic"})
     */
    private $title;

    /**
     * @ORM\Column(
     *     type="text",
     *     nullable=false,
     *     options={"default":""}
     * )
     *
     * @Groups({"basic"})
     */
    private $description = '';

    /**
     * @Assert\Length(max=255)
     * @ORM\Column(
     *     type="string",
     *     length=255,
     *     nullable=false,
     *     options={"default":""}
     * )
     *
     * @Groups({"basic"})
     */
    private $address = '';

    /**
     * @Assert\Length(max=255)
     * @ORM\Column(
     *     type="string",
     *     length=255,
     *     nullable=false,
     *     options={"default":""}
     * )
     *
     * @Groups({"basic"})
     **/
    private $phone = '';

    /**
     * @Assert\Length(max=255)
     * @ORM\Column(
     *     type="string",
     *     length=255,
     *     nullable=false,
     *     options={"default":""}
     * )
     *
     * @Groups({"basic"})
     **/
    private $workHours = '';

    /**
     * @Assert\Length(max=255)
     * @ORM\Column(
     *     type="string",
     *     length=255,
     *     nullable=false,
     *     options={"default":""}
     * )
     *
     * @Groups({"basic"})
     **/
    private $mail = '';

    /**
     * @Assert\Length(max=255)
     * @ORM\Column(
     *     type="string",
     *     length=255,
     *     nullable=false,
     *     options={"default":""}
     * )
     *
     * @Groups({"basic"})
     **/
    private $latitude = '';

    /**
     * @Assert\Length(max=255)
     * @ORM\Column(
     *     type="string",
     *     length=255,
     *     nullable=false,
     *     options={"default":""}
     * )
     *
     * @Groups({"basic"})
     **/
    private $longitude = '';


    /**
     * @Assert\Length(max=255)
     * @Asserts\UniqueField()
     * @ORM\Column(
     *     type="string",
     *     length=255,
     *     nullable=false
     * )
     *
     * @Groups({"basic"})
     **/
    private $path;

    /**
     * @Assert\Type(
     *     type="lst\CoreBundle\Entity\City"
     * )
     *
     * @ORM\ManyToOne(targetEntity="lst\CoreBundle\Entity\City")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"city"})
     **/
    private $city;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getWorkHours(): string
    {
        return $this->workHours;
    }

    /**
     * @param string $workHours
     */
    public function setWorkHours(string $workHours): void
    {
        $this->workHours = $workHours;
    }

    /**
     * @return string
     */
    public function getMail(): string
    {
        return $this->mail;
    }

    /**
     * @param string $mail
     */
    public function setMail(string $mail): void
    {
        $this->mail = $mail;
    }

    /**
     * @return string
     */
    public function getLatitude(): string
    {
        return $this->latitude;
    }

    /**
     * @param string $latitude
     */
    public function setLatitude(string $latitude): void
    {
        $this->latitude = $latitude;
    }

    /**
     * @return string
     */
    public function getLongitude(): string
    {
        return $this->longitude;
    }

    /**
     * @param string $longitude
     */
    public function setLongitude(string $longitude): void
    {
        $this->longitude = $longitude;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }

    /**
     * @return City
     */
    public function getCity(): City
    {
        return $this->city;
    }

    /**
     * @param City $city
     */
    public function setCity(City $city): void
    {
        $this->city = $city;
    }
}