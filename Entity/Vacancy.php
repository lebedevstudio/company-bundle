<?php

declare(strict_types=1);

namespace lst\CompanyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use lst\CoreBundle\Abstractions\AbstractEntity;
use lst\CoreBundle\Abstractions\Traits\Activity;
use lst\CoreBundle\Abstractions\Traits\ExternalId;
use lst\CoreBundle\Abstractions\Traits\Timestampable;
use lst\CoreBundle\Abstractions\Traits\Translatable;
use lst\CoreBundle\Interfaces\EntityTypeInterface;
use lst\CoreBundle\Validator\Constraints as Asserts;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Company Vacancy Entity
 *
 * @ORM\Table(name="company_vacancies")
 * @ORM\Entity(repositoryClass="lst\CompanyBundle\Repository\VacancyRepository")
 */
class Vacancy extends AbstractEntity implements EntityTypeInterface
{
    use Timestampable, Activity, ExternalId, Translatable;

    /** @var int */
    protected const ENTITY_TYPE_ID = 8;
    /** @var string */
    public const SINGLE_KEY = 'vacancy';
    /** @var string */
    public const MULTIPLE_KEY = 'vacancies';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"basic"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max=255
     * )
     * @Groups({"basic"})
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=false, options={"default":""})
     * @Groups({"basic"})
     */
    private $description = '';

    /**
     * @ORM\Column(type="integer", nullable=false, options={"default":0})
     * @Groups({"basic"})
     */
    private $salaryFrom = 0;

    /**
     * @ORM\Column(type="integer", nullable=false, options={"default":0})
     * @Groups({"basic"})
     */
    private $salaryTo = 0;

    /**
     * @ORM\Column(type="string", nullable=false, options={"default":""})
     * @Assert\Length(
     *     max=12
     * )
     * @Groups({"basic"})
     */
    private $phone = '';

    /**
     * @ORM\Column(type="string", nullable=false, options={"default":""})
     * @Assert\Length(
     *     max=255
     * )
     * @Groups({"basic"})
     */
    private $address = '';

    /**
     * @ORM\Column(type="string", nullable=false, options={"default":""})
     * @Assert\Length(
     *     max=255
     * )
     * @Groups({"basic"})
     */
    private $experience = '';

    /**
     * @ORM\Column(type="string", options={"default":""})
     * @Assert\Email()
     * @Groups({"basic"})
     */
    private $email = '';

    /**
     * @return int
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getSalaryFrom()
    {
        return $this->salaryFrom;
    }

    /**
     * @param mixed $salaryFrom
     */
    public function setSalaryFrom($salaryFrom): void
    {
        $this->salaryFrom = $salaryFrom;
    }

    /**
     * @return mixed
     */
    public function getSalaryTo()
    {
        return $this->salaryTo;
    }

    /**
     * @param mixed $salaryTo
     */
    public function setSalaryTo($salaryTo): void
    {
        $this->salaryTo = $salaryTo;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address): void
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getExperience()
    {
        return $this->experience;
    }

    /**
     * @param mixed $experience
     */
    public function setExperience($experience): void
    {
        $this->experience = $experience;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }
}
