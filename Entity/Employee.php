<?php

declare(strict_types=1);

namespace lst\CompanyBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use lst\CoreBundle\Abstractions\AbstractEntity;
use lst\CoreBundle\Abstractions\Traits\Activity;
use lst\CoreBundle\Abstractions\Traits\ExternalId;
use lst\CoreBundle\Abstractions\Traits\Timestampable;
use lst\CoreBundle\Abstractions\Traits\Translatable;
use lst\CoreBundle\Interfaces\EntityTypeInterface;
use lst\CoreBundle\Validator\Constraints as Asserts;
use lst\MediaBundle\Entity\File;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(name="company_employees")
 * @ORM\Entity(repositoryClass="lst\CompanyBundle\Repository\EmployeeRepository")
 */
class Employee extends AbstractEntity implements EntityTypeInterface
{
    use Timestampable, Activity, ExternalId, Translatable;

    /** @var int */
    protected const ENTITY_TYPE_ID = 15;
    /** @var string */
    public const SINGLE_KEY = 'employee';
    /** @var string */
    public const MULTIPLE_KEY = 'employees';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"basic"})
     */
    private $id;

    /**
     * @ORM\Column(
     *     type="string",
     *     length=1000
     * )
     *
     * @Assert\NotBlank()
     * @Groups({"basic"})
     */
    private $title;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max=255
     * )
     * @Asserts\UniqueField(
     *     with="locale"
     * )
     * @ORM\Column(
     *     type="string",
     *     length=255,
     *     nullable=false,
     *     options={"default":""}
     * )
     *
     * @Groups({"basic"})
     */
    protected $alias;

    /**
     * @Assert\Valid()
     * @ORM\ManyToOne(
     *     targetEntity="\lst\MediaBundle\Entity\File"
     * )
     *
     * @Groups({"basic"})
     */
    private $image = null;

    /**
     * @ORM\Column(
     *     type="text",
     *     nullable=false,
     *     options={"default":""}
     * )
     *
     * @Groups({"basic"})
     */
    private $description = '';

    /**
     * @ORM\Column(
     *     type="text",
     *     nullable=false,
     *     options={"default":""}
     * )
     *
     * @Assert\Length(max=255)
     * @Groups({"basic"})
     */
    private $position = '';

    /**
     * @ORM\Column(
     *     type="datetime_immutable",
     *     nullable=true
     * )
     *
     * @Groups({"basic"})
     */
    private $dateOfEmployment = null;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Department",
     *     inversedBy="employees"
     * )
     *
     * @Groups({"departments"})
     */
    private $departments;

    public function __construct()
    {
        $this->departments = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
    }
    /**
     * @return int|null
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id) : void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title;
    }

    /**
     * @return File|null
     */
    public function getImage() : ?File
    {
        return $this->image;
    }

    /**
     * @param File $image
     */
    public function setImage(?File $image): void
    {
        $this->image = $image;
    }


    /**
     * @param string $title
     */
    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getAlias() : string
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     */
    public function setAlias(string $alias): void
    {
        $this->alias = $alias;
    }
    
    /**
     * @return string
     */
    public function getDescription() : string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description) : void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getPosition() : string
    {
        return $this->position;
    }

    /**
     * @param string $position
     */
    public function setPosition(string $position) : void
    {
        $this->position = $position;
    }

    /**
     * @return \DateTimeImmutable|null
     */
    public function getDateOfEmployment() : ?string
    {
        return $this->dateOfEmployment != null ? $this->dateOfEmployment->format('c') : null;
    }

    /**
     * @param string $dateOfEmployment
     */
    public function setDateOfEmployment(?\DateTimeImmutable $dateOfEmployment): void
    {
        $this->dateOfEmployment = $dateOfEmployment;
    }

    /**
     * @return Collection|Department[]
     */
    public function getDepartments(): Collection
    {
        return $this->departments;
    }
}
