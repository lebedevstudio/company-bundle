<?php

declare(strict_types=1);

namespace lst\CompanyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use lst\CoreBundle\Abstractions\AbstractEntity;
use lst\CoreBundle\Abstractions\Traits\Timestampable;
use lst\CoreBundle\Interfaces\EntityTypeInterface;
use Symfony\Component\Validator\Constraints as Assert;
use lst\CoreBundle\Validator\Constraints as Asserts;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Company Feedback Type Entity
 * @ORM\Table(name="company_feedback_type")
 * @ORM\Entity(repositoryClass="lst\CompanyBundle\Repository\FeedbackTypeRepository")
 */
class FeedbackType extends AbstractEntity implements EntityTypeInterface
{
    use Timestampable;

    /** @var int */
    protected const ENTITY_TYPE_ID = 8;
    /** @var string */
    public const SINGLE_KEY = 'feedbackType';
    /** @var string */
    public const MULTIPLE_KEY = 'feedbackTypes';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"basic"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max=255
     * )
     * @Groups({"basic"})
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *     max=255
     * )
     * @Groups({"basic"})
     */
    private $description = '';

    /**
     * @ORM\Column(type="string", length=255, nullable=false, options={"default":""})
     * @Asserts\UniqueField()
     * @Assert\Length(
     *     max=255
     * )
     * @Groups({"basic"})
     */
    private $path = '';

    /**
     * @return int|null
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }
}
