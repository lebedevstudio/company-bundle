<?php

declare(strict_types=1);

namespace lst\CompanyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use lst\CoreBundle\Abstractions\AbstractEntity;
use lst\CoreBundle\Abstractions\Traits\Timestampable;
use lst\CoreBundle\Abstractions\Traits\Translatable;
use lst\CoreBundle\Interfaces\EntityTypeInterface;
use lst\MediaBundle\Entity\File;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Company Feedback Entity
 * @ORM\Table(name="company_feedback")
 * @ORM\Entity(repositoryClass="lst\CompanyBundle\Repository\FeedbackRepository")
 */
class Feedback extends AbstractEntity implements EntityTypeInterface
{
    use Timestampable, Translatable;

    /** @var int */
    protected const ENTITY_TYPE_ID = 7;
    /** @var string */
    public const SINGLE_KEY = 'feedback';
    /** @var string */
    public const MULTIPLE_KEY = 'feedbacks';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"basic"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max=255
     * )
     * @Groups({"basic"})
     */
    private $caller;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank()
     * @Groups({"basic"})
     */
    private $content = '';

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Length(
     *     max=12
     * )
     * @Groups({"basic"})
     */
    private $phone = '';

    /**
     * @ORM\Column(type="string")
     * @Assert\Email()
     * @Assert\NotBlank()
     * @Groups({"basic"})
     */
    private $email;

    /**
     * @ORM\Column(type="boolean", options={"default":false})
     * @Groups({"basic"})
     */
    private $isRead = false;

    /**
     * @ORM\ManyToOne(targetEntity="lst\CompanyBundle\Entity\FeedbackType")
     * @Assert\Valid()
     * @Groups({"basic"})
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="lst\MediaBundle\Entity\File")
     * @Groups({"basic"})
     */
    private $attach = null;

    /**
     * @ORM\Column(type="json", options={"default":"{}"})
     * @Groups({"basic"})
     */
    private $extra = [];

    /**
     * @return int|null
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id) : void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCaller() : string
    {
        return $this->caller;
    }

    /**
     * @param string $caller
     */
    public function setCaller(string $caller) : void
    {
        $this->caller = $caller;
    }

    /**
     * @return string
     */
    public function getContent() : string
    {
        return $this->content;
    }

    /**
     * @param string $description
     */
    public function setContent(string $content) : void
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getPhone() : string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone) : void
    {
        $this->phone = $phone;
    }

    /**
     * @return strin
     */
    public function getEmail() : string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email) : void
    {
        $this->email = $email;
    }

    /**
     * @return bool
     */
    public function isRead() : bool
    {
        return $this->isRead;
    }

    /**
     * @param bool $isRead
     */
    public function setIsRead(bool $isRead) : void
    {
        $this->isRead = $isRead;
    }

    /**
     * @return FeedbackType
     */
    public function getType() : FeedbackType
    {
        return $this->type;
    }

    /**
     * @param FeedbackType $type
     */
    public function setType(FeedbackType $type): void
    {
        $this->type = $type;
    }

    /**
     * @param File|null $file
     */
    public function setAttach(?File $file) : void
    {
        $this->attach = $file;
    }

    /**
     * @return File|null
     */
    public function getAttach() : ?File
    {
        return $this->attach;
    }

    /**
     * @param array $extra
     */
    public function setExtra(array $extra) : void
    {
        $this->extra = $extra;
    }

    /**
     * @return array
     */
    public function getExtra() : array
    {
        return ($this->extra == '') ? [] : $this->extra;
    }
}
