<?php

declare(strict_types=1);

namespace lst\CompanyBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use lst\CoreBundle\Abstractions\AbstractEntity;
use lst\CoreBundle\Abstractions\Traits\Activity;
use lst\CoreBundle\Abstractions\Traits\Timestampable;
use lst\CoreBundle\Abstractions\Traits\Translatable;
use lst\CoreBundle\Interfaces\EntityTypeInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Department Entity
 *
 * @ORM\Table(name="company_departments")
 * @ORM\Entity(repositoryClass="lst\CompanyBundle\Repository\DepartmentRepository")
 */
class Department extends AbstractEntity implements EntityTypeInterface
{
    use Timestampable, Activity, Translatable;

    /** @var int */
    protected const ENTITY_TYPE_ID = 25;
    /** @var string */
    public const SINGLE_KEY = 'department';
    /** @var string */
    public const MULTIPLE_KEY = 'departments';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups({"basic"})
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     * @Assert\Type(type="string")
     *
     * @ORM\Column(
     *     type="string",
     *     length=255,
     *     nullable=false
     * )
     *
     * @Groups({"basic"})
     */
    private $title;

    /**
     * @ORM\Column(
     *     type="text",
     *     nullable=false,
     *     options={"default":""}
     * )
     *
     * @Groups({"basic"})
     */
    private $description = '';

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Employee",
     *     mappedBy="departments"
     * )
     *
     * @Groups({"employees"})
     */
    private $employees;

    public function __construct()
    {
        $this->employees = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id) : void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param  string  $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return Collection|Employee[]
     */
    public function getEmployees(): Collection
    {
        return $this->employees;
    }
}