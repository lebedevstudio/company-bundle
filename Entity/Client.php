<?php

declare(strict_types=1);

namespace lst\CompanyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use lst\CoreBundle\Abstractions\AbstractEntity;
use lst\CoreBundle\Abstractions\Traits\Activity;
use lst\CoreBundle\Abstractions\Traits\ExternalId;
use lst\CoreBundle\Abstractions\Traits\Timestampable;
use lst\CoreBundle\Abstractions\Traits\Translatable;
use lst\CoreBundle\Interfaces\EntityTypeInterface;
use lst\CoreBundle\Validator\Constraints as Asserts;
use lst\MediaBundle\Entity\File;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Company Client Entity
 *
 * @ORM\Table(name="company_clients")
 * @ORM\Entity(repositoryClass="lst\CompanyBundle\Repository\ClientRepository")
 */
class Client extends AbstractEntity implements EntityTypeInterface
{
    use Timestampable, Activity, ExternalId, Translatable;

    /** @var int */
    protected const ENTITY_TYPE_ID = 16;
    /** @var string */
    public const SINGLE_KEY = 'client';
    /** @var string */
    public const MULTIPLE_KEY = 'clients';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"basic"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max=255
     * )
     * @Groups({"basic"})
     */
    private $title;

    /**
     * @ORM\Column(
     *     type="string",
     *     length=255,
     *     nullable=false,
     *     options={"default":""}
     * )
     * @Assert\NotNull()
     * @Assert\Length(max=255)
     * @Groups({"basic"})
     */
    private $subtitle = '';

    /**
     * @ORM\Column(
     *     type="text",
     *     nullable=false,
     *     options={"default":""}
     * )
     * @Groups({"basic"})
     */
    private $description = '';

    /**
     * @Assert\Valid()
     * @ORM\ManyToOne(targetEntity="lst\MediaBundle\Entity\File")
     * @Groups({"basic"})
     */
    private $image = null;

    /**
     * @ORM\Column(
     *     type="string",
     *     length=255,
     *     options={"default":""}
     * )
     * @Assert\Length(max=255)
     * @Groups({"basic"})
     */
    private $link = '';

    /**
     * @ORM\Column(
     *     type="text",
     *     nullable=false,
     *     options={"default":""}
     * )
     * @Groups({"basic"})
     */
    private $review = '';

    /**
     * @return int|null
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id) : void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getSubtitle() : string 
    {
        return $this->subtitle;
    }

    /**
     * @param string $subtitle
     */
    public function setSubtitle(string $subtitle) : void
    {
        $this->subtitle = $subtitle;
    }

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description) : void
    {
        $this->description = $description;
    }

    /**
     * @return File|null
     */
    public function getImage() : ?File
    {
        return $this->image;
    }

    /**
     * @param File|null $image
     */
    public function setImage(?File $image) : void
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getLink() : string
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink(string $link) : void
    {
        $this->link = $link;
    }

    /**
     * @return string
     */
    public function getReview(): string
    {
        return $this->review;
    }

    /**
     * @param string $review
     */
    public function setReview(string $review): void
    {
        $this->review = $review;
    }
}
